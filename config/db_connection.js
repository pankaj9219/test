

const winston = require('./logger');
const mongoose = require('mongoose');
var url;


// Set up default mongoose connection

let database = '';
const options = {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
let port;
if (process.env.NODE_ENV == 'developement') {
  database = process.env.DB_LOCAL;
  // options.user = process.env.DB_USER;
  // options.pass = process.env.DB_PASS;
  // options.authSource = process.env.DB_AUTH_SOURCE
  port = process.env.DB_LOCAL_PORT;
} else {
  database = process.env.DB_PROD;
  // options.user = process.env.DB_PROD_USER;
  // options.pass = process.env.DB_PROD_PASS;
  port = process.env.DB_PROD_PORT;
  // options.authSource=process.env.DB_AUTH_SOURCE_PROD
}
console.log(database)
url = `mongodb://${port}:27017/${database}`;
console.log(url)
mongoose.connect(url, options).then((client, err) => {
  if (err) {
    winston.error(err);
  } else {
    const obj = {
      DB_NAME: client.connections[0].name,
      HOST: client.connections[0].host,
      PORT: client.connections[0].port,
    };

    winston.debug(`Connected to db`);
    winston.info(obj);
  }
}).catch((err) => {
    console.log(err)
  winston.error(err);
});
