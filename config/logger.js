const winston = require('winston');
const {format} = winston;
const {combine, timestamp, label, printf} = format;
const appRoot = require('app-root-path');
// define the custom settings for each transport (file, console)
const options = {
  file: {
    level: 'info',
    filename: `${appRoot}/logs/app.log`,
    handleExceptions: true,
    json: true,
    colorize: false,
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};
const myFormat = printf(({level, message, label, timestamp}) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});
winston.addColors({info: 'cyan', error: 'red', warn: 'yellow', debug: 'green'});
// instantiate a new Winston Logger with the settings defined above
const logger = winston.createLogger({
  format: combine(
      label({label: 'log'}),
      timestamp(),
      winston.format.colorize({all: true}),
      myFormat,
  ),
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console),
  ],
  exitOnError: false, // do not exit on handled exceptions
});

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
  write: function(message, encoding) {
    logger.info(message);
  },
};

module.exports = logger;
