/*
 * @file: routes.js
 * @description: This is the route configration file for user's route
 * @author: Pankaj kumar
*/

const express = require('express');
const router = express.Router();
const userService = require('../../controllers/user/service');
const base_route = '/api/v1/user/'
const Joi = require('joi');
const userModel = require('../../model/users.model');
const jobdeuserModel = require('../../model/jobdeuser.model');

const messages = require('../../common/commonMessages').customMessages()
const jwt = require('jsonwebtoken')
const jobdeservice = require('../../controllers/user/jobde.user')
// const userModel = require('../../models/users.model');
// var formidable = require('formidable');
// var commonFunc = require('../commonFunction');
// const userModel = require('../../models/users.model')
//  user routes configration
router.post(`${base_route}register`, Authenticate,userService.register);
router.post(`${base_route}resend-otp`, userService.resendOtp);
router.post(`${base_route}verify-otp`,  userService.verifyOtp);
router.post(`${base_route}check/email-or-mobile`, userService.createUser);
router.post(`${base_route}upload-documents`, Authenticate,userService.uploadDocument);
router.get(`${base_route}get-skills`,userService.getProfessionType);
router.get(`${base_route}get-sub-categories`,Authenticate,userService.getSubCategories);
router.get(`${base_route}get-categories`,Authenticate,userService.getCategories);
router.post(`${base_route}login`,userService.logIn);
router.post(`${base_route}check-username`,Authenticate,userService.checkUserName);
router.post(`${base_route}add-experience`,Authenticate,userService.saveExperince);
router.post(`${base_route}add-education`,Authenticate,userService.uploadEducationDocs);
router.post(`${base_route}add-salary`,Authenticate,userService.salaryExpectation);
router.post(`${base_route}change-password`,userService.changePassword);
router.put(`${base_route}save-categories`,Authenticate,userService.saveCategories);
router.post(`${base_route}user-list`,Authenticate, userService.userList);
router.get(`${base_route}user-profile`,Authenticate, userService.userProfile);
router.get(`${base_route}home-page`,userService.homePage);






/********************************** Job de app routes *******************************/
router.post(`${base_route}job-de/register`,jobdeservice.registerUser);
router.post(`${base_route}job-de/login`,jobdeservice.login);
router.post(`${base_route}job-de/forgot-password`,jobdeservice.forgotPwd);
router.post(`${base_route}job-de/resend-otp`,jobdeservice.resendOtp);
router.post(`${base_route}job-de/verify-otp`,jobdeservice.verifyOtp);
router.post(`${base_route}job-de/reset-password`,jobdeservice.resetPassword);
router.post(`${base_route}job-de/create-post`,Authenticate,jobdeservice.createPost);
router.get(`${base_route}job-de/user-profile`,Authenticate,jobdeservice.userProfile);
router.get(`${base_route}job-de/home`,Authenticate,jobdeservice.homePage);
router.get(`${base_route}job-de/posts`,Authenticate,jobdeservice.getPosts);
router.get(`${base_route}job-de/get-post-by-id`,Authenticate,jobdeservice.getPostById);
router.put(`${base_route}job-de/update/post/:id`,jobdeservice.updatePost);
router.put(`${base_route}job-de/update/user-profile/:id`,jobdeservice.updateProfile);








async function Authenticate(req, res, next) {
    // console.log(req.path.split('/'))
    const path = req.path.split('/')[4]
    console.log(path)
    let tokenStr = req.headers['x-token'] || req.query['token']
    // console.log( path == 'job-de')

    let readToken = path == 'job-de'? await jobdeuserModel.findOne({ accessToken: tokenStr }):await userModel.findOne({ accessToken: tokenStr });
    // console.log( readToken,"tokeneee")
    
    if (!readToken) return res.send({ status: false, code: 401, message: messages.SESSION_ERROR })
    if (tokenStr == readToken.accessToken) {
        console.log('-----------------------------------------')
      jwt.verify(tokenStr, process.env.TOKEN_SECRET, function (err, result) {
        if (err) {
            return res.status(401).send({ status: false, code: 401, message:messages.SESSION_ERROR })
        }
        else {
            // console.log(readToken,"0000")
            req.body.userInfo = readToken;
            next();
        }
      })
    } else {
      return res.status(401).send({ status: false, code: 203, message: messages.SESSION_ERROR })
    }
  }



// Middleware
function joiValidation(req, res, next) {
    //register params
    console.log(req.body, "========dashfsdfk")
    const registrationSchema = Joi.object({
        "fullName": Joi.string().min(3).max(50).required(),
        "email": Joi.string().email().trim().lowercase().required(),
        "mobileNumber": Joi.string().trim().regex(/^[0-9]{7,10}$/).required().optional().allow(''),
        "countryCode": Joi.string().required().optional().allow(''),
        "countryCodeText": Joi.string().required().optional().allow(''),
        "password": Joi.string().min(4).max(15).required(),
        "permanentAddress": Joi.string().required(),
        "permanentState": Joi.string().required(),
        // "permanentZipCode": Joi.number().required(),
        "presentAddress": Joi.string().required(),
        "presentState": Joi.string().required(),
        // "presentZipCode": Joi.string().required(),
        "isTermAndPolicy": { type: Boolean, default: false },
        "deviceType": Joi.string().required(),
        "deviceToken": Joi.string().required(),
        "deviceModel": Joi.string().required(),
        "appVersion": Joi.string().required(),
    })
    const resendOtp = Joi.object({ "mobileNumber": Joi.string().required(), "countryCode": Joi.string().required() });
    const verifyOtp = Joi.object({ "mobileNumber": Joi.string().required(), "countryCode": Joi.string().required(), "otp": Joi.string().required() });
    const forgotPassword = Joi.object({ "email": Joi.string().required() });
    const changePassword = Joi.object({ "email": Joi.string().required(), "userId": Joi.string().required(), "oldPassword": Joi.string().required(), "newPassword": Joi.string().required() });
    const getProfile = Joi.object({ "userId": Joi.string().required(), });
    const profileSet = Joi.object({
        "userId": Joi.string().required(),
        "age": Joi.string().required(),
        "gender": Joi.string().required(),
        "profession": Joi.string().required(),
        "hobbies": Joi.string().required(),
        "bio": Joi.string().required(),
        "lat": Joi.string().required(),
        "long": Joi.string().required(),
        "location": Joi.string().optional().allow(),
        "screenType": Joi.string().required()
    });
    const helpAndSupport = Joi.object({
        "userId": Joi.string().required(),
        "title": Joi.string().required(),
        "description": Joi.string().required()
    });
    const notificationList = Joi.object({ 'userId': Joi.string().required(), "type": Joi.string().required() });
    const notificationOnOff = Joi.object({ 'userId': Joi.string().required(), "type": Joi.string().required(), "notificationType": Joi.string().required() });
    // schema options
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    var profilePreferences = Joi.object({
        "userId": Joi.string().required(),
        "intrestedIn": Joi.string().required(),
        "ageRangeStart": Joi.number().required(),
        "ageRangeEnd": Joi.number().required(),
        "location": Joi.string().required(),
        "showProfessions": Joi.string().required(),
        "screenType": Joi.string().required()
    })

    const loginCred = Joi.object({
        "email": Joi.string().email().trim().lowercase().optional().allow(''),
        "password": Joi.string().optional().allow(''),
        "deviceType": Joi.string().required(),
        "deviceToken": Joi.string().required(),
        "deviceModel": Joi.string().required(),
        "appVersion": Joi.string().required(),
    })

    if (req.path == '/user/auth/login') var { error, value } = loginCred.validate(req.body, options);

    if (req.path == `${base_route}resend-otp`) var { error, value } = resendOtp.validate(req.body, options);
    if (req.path == `${base_route}register`) var { error, value } = registrationSchema.validate(req.body, options);
    if (req.path == `${base_route}verify-otp`) var { error, value } = verifyOtp.validate(req.body, options);
    if (req.path == '/user/forgot-password') var { error, value } = forgotPassword.validate(req.body, options);
    if (req.path == '/user/change-password') var { error, value } = changePassword.validate(req.body, options);
    if (req.path == '/user/get-profile') var { error, value } = getProfile.validate(req.body, options);
    if (req.path == '/user/profile-set/basic') var { error, value } = profileSet.validate(req.body, options);
    if (req.path == '/user/help-and-support') var { error, value } = helpAndSupport.validate(req.body, options);
    if (req.path == '/user/notification/list') var { error, value } = notificationList.validate(req.body, options);
    if (req.path == '/user/notification/on-off') var { error, value } = notificationOnOff.validate(req.body, options);
    // if(req.path == '/user/profile-set/preferences') var { error, value } = profilePreferences.validate(req.body, options);

    if (error) {
        // on fail return comma separated errors
        res.send({ status: false, code: 201, message: `${error.details.map(x => x.message.replace(/"/g, ''))[0]}` });
        // next(`Validation error: ${error.details.map(x => x.message).join(', ')}`);
    } else {
        // on success replace req.body with validated value and trigger next middleware function
        req.body = value;
        next();
    }
}



/**
 * @swagger
 * /api/v1/user/register:
 *   post:
 *     summary: register user
 *     description: mobileNumber,countryCode and CountryCodeText is optional.
 *     tags: [users]
 *     parameters:
 *      - in: header
 *        name: x-token
 *        schema:
 *          type: string
 *        required: true
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *              fullName:
 *                type: string
 *              email:
 *                type: string
 *              password:
 *                type: string
 *              mobileNumber:
 *                type: string
 *              countryCode:
 *                type: string
 *              countryCodeText:
 *                type: string
 *              deviceType:
 *                type: string
 *              permanentAddress:
 *                type: string
 *              permanentState:
 *                type: string
 *              permanentZipCode:
 *                type: integer
 *              presentAddress:
 *                type: string
 *              presentState:
 *                type: string
 *              presentZipCode:
 *                type: integer
 *              isTermAndPolicy:
 *                type: boolean
 *              deviceToken:
 *                type: string
 *              deviceModel:
 *                type: string
 *              appVersion:
 *                type: string
 *
 *     responses:
 *       200:
 *         description: user was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *       500:
 *         description: Some server error
 */

/**
* @swagger
* /api/v1/user/resend-otp:
*   post:
*     summary: resend otp
*     description: mobileNumber and email are optional if mobileNumber you want country code is required
*     tags: [users]
*     requestBody:
*       required: true
*       content:
*         application/json:
*           schema:
*             type: object
*             properties:
*              mobileNumber:
*                type: string
*              email:
*                type: string
*     responses:
*       200:
*         description: resend otp
*         content:
*           application/json:
*             schema:
*               type: object
*       500:
*         description: Some server error
*/

/**
 * @swagger
 * /api/v1/user/verify-otp:
 *   post:
 *     summary: Verifi-otp
 *     description: mobileNumber,countryCode and otp is required
 *     tags: [users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *              mobileNumber:
 *                type: string
 *                required: true
 *              countryCode:
 *                type: string
 *                required: true
 *              otp:
 *                type: string
 *                required: true
 *
 *     responses:
 *       200:
 *         description: Verified successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *       500:
 *         description: Some server error
 */

 /**
 * @swagger
 * /api/v1/user/check/email-or-mobile:
 *   post:
 *     summary: email-or-mobile
 *     description: mobileNumber,countryCode and otp is required
 *     tags: [users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *              mobileNumber:
 *                type: string
 *                required: true
 *              countryCode:
 *                type: string
 *                required: true
 *              email:
 *                type: string
 *                required: true
 *
 *     responses:
 *       200:
 *         description: Verified successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *       500:
 *         description: Some server error
 */


 /**
 * @swagger
 * /api/v1/user/upload-documents:
 *   post:
 *     summary: /api/v1/user/upload-documents
 *     description: userId,age,gender,professsion,hobbies,bio and screenType is required
 *     tags: [users]
 *     parameters:
 *      - in: header
 *        name: x-token
 *        schema:
 *          type: string
 *        required: true
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             type: object
 *             properties:
 *              file:
 *                type: array
 *                items:
 *                 type: string
 *                 format: binary
 *
 *     responses:
 *       200:
 *         description: basic profile set successfully
 *         content:
 *           multipart/form-data:
 *             schema:
 *               type: object
 *       500:
 *         description: Some server error
 */
module.exports = router;
