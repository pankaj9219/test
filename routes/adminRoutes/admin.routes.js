/**
 * file:admin.routes.js
 * desc: this file contain all the routes for admin
 * date:2-july-2021
 */
const express = require('express');
const router = express.Router();
const adminFile = require('../../controllers/admin/service');
const base_route = '/api/v1/admin/'
// admin route configration

router.post(`${base_route}add/category`, adminFile.addCategories);
router.post(`${base_route}add/sub-category`, adminFile.addSubcategory);
router.post(`${base_route}get/categories`, adminFile.getAllCategories);
router.post(`${base_route}edit/category`, adminFile.editCategory);
router.post(`${base_route}edit/subcat`, adminFile.editSubCat);
router.post(`${base_route}auth`, adminFile.checkEmail);
router.post(`${base_route}resend-otp`, adminFile.resendOtp);
router.post(`${base_route}verify-otp`, adminFile.verifyOtp);
router.delete(`${base_route}delete/:id`, adminFile.deletCategory);
router.get(`${base_route}get-category`, adminFile.getCategory);
router.get(`${base_route}get-sub-categories`, adminFile.getSubCategories);
router.get(`${base_route}block-user`, adminFile.blockUser);
router.get(`${base_route}unblock-user`, adminFile.unBlockUser);
router.get(`${base_route}delete-user`, adminFile.deleteUser);
router.get(`${base_route}get-users`, adminFile.getAllUsers);
router.get(`${base_route}get-user-detail`, adminFile.getUserdetail);
router.get(`${base_route}get-sub-category`,adminFile.getSubCategory)
router.delete(`${base_route}delete/sub-category/:id`, adminFile.deleteSubCat);

router.post(`${base_route}upload/banner`,adminFile.uploadBanner);
router.post(`${base_route}create-skill`,adminFile.createSkills);


module.exports = router;
