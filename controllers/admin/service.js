/**
 * File Name: service.js
 * Date: 29-june-2021
 * Desc: this file contain all the logics for the admin panel
 */

// model configration
const categoryModel = require("../../model/category.model");
const subCategoryModel = require("../../model/subcategory.model");
const adminModel = require("../../model/admin.model");
const userModel = require("../../model/users.model");
const bannerModel = require("../../model/banner.model");
const skillsModel = require('../../model/skills.model')

//file configration
const logger = require("../../config/logger");
const messages = require("../../common/commonMessages").customMessages();
const commnFunction = require("../../common/commonFunction");
const jwt = require("jsonwebtoken");
const async = require("async");

module.exports = {
  //check email exist or not
  checkEmail: async (req, res) => {
    let body = req.body;
    try {
      let user = await adminModel.findOne({ email: body.email });
      if (!user)
        return res.send({
          status: false,
          code: 400,
          message: `${body.email} ${messages.NOT_FOUND}`,
        });
      console.log(user.password, commnFunction.encrypt(body.password));
      if (user.password == commnFunction.encrypt(body.password)) {
        res.send({ status: true, code: 200, message: "" });
      } else {
        res.send({ status: false, code: 401, message: messages.NOT_MATCHED });
      }
    } catch (err) {
      logger.error(err);
      res.send({ status: false, code: 400, message: messages.BAD_REQUEST });
    }
    res.end();
  },

  // send otp for login
  resendOtp: async (req, res) => {
    let body = req.body;
    try {
      let isEmail = await adminModel.findOne({ email: body.email });
      if (!isEmail)
        return res.send({
          statptus: false,
          code: 400,
          message: `${body.email} ${messages.NOT_FOUND}`,
        });
      let otp = commnFunction.genrateOtp(6);
      try {
        await adminModel.updateOne(
          { email: body.email },
          { $set: { otp: otp } }
        );
      } catch (err) {
        logger.error(err);
        return res.send({
          status: false,
          code: 400,
          message: messages.BAD_REQUEST,
        });
      }
      let htmlStr = `<p> <em> Hi ${isEmail.firstName} ${isEmail.lastName},  </em> </p>
                          <p> Your login otp is ${otp} </p>
                          <p> Thanks & Regards  </p>
                          <p>Team JobLe </p>`;

      try {
        await commnFunction.sendMail(body.email, "Otp Verification", htmlStr);
        return res.send({
          status: true,
          code: 200,
          message: "Otp send successfully on your given email",
        });
      } catch (err) {
        logger.error(err);
        return res.send({
          status: false,
          code: 400,
          message: "Email not send sucessfully",
        });
      }
    } catch (err) {
      logger.error(err);
      return res.send({
        status: false,
        code: 400,
        message: messages.BAD_REQUEST,
      });
    }
  },

  //verify otp
  verifyOtp: async (req, res) => {
    let body = req.body;
    console.log(body);
    try {
      let isEmail = await adminModel.findOne({ email: body.email });
      if (!isEmail)
        return res.send({
          status: false,
          code: 400,
          message: ` <strong> ${body.email} </strong>  is  ${messages.NOT_FOUND}`,
        });
      if (isEmail.otp == body.otp) {
        let token = jwt.sign({ email: body.email }, process.env.TOKEN_SECRET, {
          expiresIn: "1h",
        });
        try {
          await adminModel.updateOne(
            { email: body.email },
            { $set: { otp: null } }
          );
          req.session.email = body.email;
          req.session.token = token;
        } catch (err) {
          logger.error(err);
          return res.send({
            status: false,
            code: 400,
            message: messages.ANNONYMOUS,
          });
        }
        return res.send({
          status: true,
          code: 200,
          message: messages.LOGIN,
          token: token,
        });
      } else {
        return res.send({ status: false, code: 400, message: "Invalid otp" });
      }
    } catch (err) {
      logger.error(err);
      res.send({ status: false, code: 400, message: messages.BAD_REQUEST });
    }
    res.end();
  },

  // logout user
  logOut: async (req, res) => {
    const body = req.body;
    try {
      let user = await userModel.findOne({ email: body.email });
      if (!user)
        return res.send({
          status: false,
          code: 400,
          message: messages.ANNONYMOUS,
        });

      try {
        // await userModel.updateOne({email:body.email},{$set:{}})
      } catch (error) {
        logger.error(error);
        console.log(error);
        return res.send({
          status: false,
          code: 400,
          message: messages.ANNONYMOUS,
        });
      }
    } catch (error) {
      logger.error(error);
      return res.send({ status: false, code: 400, message: messages });
    }
  },

  /* Admin will unique Add sub Category */
  addCategories: async (req, res) => {
    let body = req.body;
    console.log(capitalize(body.categoryName));
    try {
      // let isExist = await categoryModel.findOne({ categoryName: body.categoryName });
      let query = {
        $or: [
          { categoryName: capitalize(body.categoryName) },
          { categoryName: body.categoryName.toLowerCase() },
          { categoryName: body.categoryName.toUpperCase() },
          { categoryName: body.categoryName },
        ],
      };
      console.log(JSON.stringify(query));

      let isExist = await categoryModel.find(query);
      console.log(isExist);
      if (isExist.length)
        return res.send({
          status: false,
          code: 400,
          message: `${body.categoryName} Category ${messages.Exist}`,
        });
      let addCategory = await new categoryModel({
        categoryName: capitalize(body.categoryName),
        skillType: body.skillType,
      }).save();
      let response = commnFunction.checkRes(addCategory);
      response.message = messages.CATEGORY_ADDED;
      res.send(response);
    } catch (err) {
      logger.error(err);
      console.log(err);
      res.send({ status: false, code: 400, message: messages.BAD_REQUEST });
    }
    res.end();
  },

  /* Add unique Sub category with category id */
  addSubcategory: async (req, res) => {
    let body = req.body;
    body.subCategory = capitalize(body.subCategory);
    console.log(body, "----========");
    try {
      let query = {
        $or: [
          { subCategory: capitalize(body.subCategory) },
          { subCategory: body.subCategory.toLowerCase() },
          { subCategory: body.subCategory.toUpperCase() },
          { subCategory: body.subCategory },
        ],
      };
      let isSubCatExist = await subCategoryModel.findOne(query);
      if (isSubCatExist)
        return res.send({
          status: false,
          code: 400,
          message: `Sub Category ${body.subCategory} ${messages.Exist}`,
        });
      try {
        let saveSubCat = await new subCategoryModel(body).save();
        let response = commnFunction.checkRes(saveSubCat);
        response.message = `${body.subCategory} Added as ${messages.SUB_CAT_ADDED}`;
        res.send(response);
      } catch (err) {
        logger.error(err);
        res.send({ status: false, code: 400, message: messages.BAD_REQUEST });
      }
    } catch (err) {
      logger.error(err);
      res.send({ sttaus: false, code: 400, message: messages.BAD_REQUEST });
    }
    res.end();
  },

  /* Get all the categories */
  getAllCategories: async (req, res) => {
    let body = req.body;
    try {
      let categories = await categoryModel.find();
      let response = commnFunction.checkRes(categories);
      response.message = messages.FETCH;
      res.send(response);
    } catch (err) {
      logger.error(err);
      res.send({ status: false, code: 400, message: messages.BAD_REQUEST });
    }
    res.end();
  },

  /* Edit category */
  editCategory: async (req, res) => {
    let body = req.body;
    console.log(body);
    try {
      let isExist = await categoryModel.findOne({ _id: body.categoryId });
      if (!isExist)
        return res.send({
          status: false,
          code: 400,
          message: messages.CAT_NOT_FOUND,
        });
      let query = { $set: { categoryName: body.categoryName ,skillType:body.skillType} };
      try {
        let updateCat = await categoryModel.updateOne(
          { _id: body.categoryId },
          query
        );
        let response = commnFunction.checkRes(updateCat);
        response.message = messages.UPDATE;
        res.send(response);
      } catch (err) {
        logger.error(err);
        res.send({ status: false, code: 400, message: messages.BAD_REQUEST });
      }
    } catch (err) {
      logger.error(err);
      res.send({ status: false, code: 400, message: messages.BAD_REQUEST });
    }
    res.end();
  },

  /* Edit sub Category*/
  editSubCat: async (req, res) => {
    let body = req.body;
    try {
      let isExist = await subCategoryModel.findOne({ _id: body.subcatId });
      if (!isExist)
        return res.send({
          status: false,
          code: 400,
          message: messages.CAT_NOT_FOUND,
        });
      let query = { $set: { subCategory: body.subCategory } };
      try {
        let updateCat = await subCategoryModel.updateOne(
          { _id: body.subcatId },
          query
        );
        let response = commnFunction.checkRes(updateCat);
        response.message = messages.UPDATE;
        res.send(response);
      } catch (err) {
        logger.error(err);
        res.send({ status: false, code: 400, message: messages.BAD_REQUEST });
      }
    } catch (err) {
      logger.error(err);
      res.send({ status: false, code: 400, message: messages.BAD_REQUEST });
    }
    res.end();
  },

  /* Delete category based on category id*/
  deletCategory: async (req, res) => {
    const categoryId = req.params.id;
    try {
      let isExist = await categoryModel.findOne({ _id: categoryId });
      if (!isExist)
        return res.send({
          status: false,
          code: 400,
          message: `Category ${messages.NOT_FOUND}`,
        });
      let isDelete = await categoryModel.deleteOne({ _id: categoryId });
      await subCategoryModel.deleteMany({ categoryId: categoryId });
      let response = commnFunction.checkRes(isDelete);
      response.message = `${isExist.categoryName} deleted successfully`;
      res.send(response);
    } catch (err) {
      console.log(err);
      logger.error(err);
      res.send({ status: false, code: 400, message: messages.ANNONYMOUS });
    }
    res.end();
  },

  /* Get category */
  getCategory: async (req, res) => {
    const id = req.query.id;
    //get category
    try {
      let getCategory = await categoryModel.findOne({ _id: id });
      let response = commnFunction.checkRes(getCategory);
      response.message = "";
      res.send(response);
    } catch (err) {
      logger.error(err);
      res.send({ status: false, code: 201, message: messages.ANNONYMOUS });
    }
    res.end();
  },

  /* get sub categories*/
  getSubCategories: async (req, res) => {
    //get sub categories
    const id = req.query.id;

    try {
      let subCategories = await subCategoryModel.find({categoryId:id});
      let response = commnFunction.checkRes(subCategories);
      response.message = "";
      res.send(response);
    } catch (err) {
      logger.error(err);
      res.send({ status: false, code: 201, message: messages.ANNONYMOUS });
    }
    res.end();
  },

  /* delete  sub category  */
  deleteSubCat: async (req, res) => {
    const subCategoryId = req.params.id;
    try {
      let isExist = await subCategoryModel.findOne({ _id: subCategoryId });
      if (!isExist)
        return res.send({
          status: false,
          code: 400,
          message: `Category ${messages.NOT_FOUND}`,
        });
      let isDelete = await subCategoryModel.deleteOne({ _id: categoryId });
      let response = commnFunction.checkRes(isDelete);
      response.message = `Sub-category deleted successfully`;
      res.send(response);
    } catch (err) {
      console.log(err);
      logger.error(err);
      res.send({ status: false, code: 400, message: messages.ANNONYMOUS });
    }
    res.end();
  },

  /* get sub category */
  getSubCategory: async (req, res) => {
    const id = req.query.id;
    //get category
    try {
      let subCategory = await subCategoryModel.findOne({ _id: id });
      let response = commnFunction.checkRes(subCategory);
      response.message = "";
      res.send(response);
    } catch (err) {
      logger.error(err);
      res.send({ status: false, code: 201, message: messages.ANNONYMOUS });
    }
    res.end();
  },

  /* Get al users*/
  getAllUsers: async (req, res) => {
    //get users
    try {
      let query = [{ $project: { password: 0, accessToken: 0 } }];
      let users = await userModel.aggregate(query);
      let response = commnFunction.checkRes(users);
      response.message = "";
      res.send(response);
    } catch (e) {
      logger.error(e);
      res.send({ status: false, code: 201, message: messages.ANNONYMOUS });
    }
    res.end();
  },

  /*Block users*/
  blockUser: async (req, res) => {
    let id = req.query.id;
    // block users
    try {
      let blockUser = await userModel.updateOne(
        { _id: id },
        { $set: { isBlocked: true } }
      );
      let response = commnFunction.checkRes(blockUser);
      response.message = "User blocked sucessfully";
      res.send(response);
    } catch (e) {
      logger.error(e);
      res.send({ status: false, code: 201, message: messages.ANNONYMOUS });
    }
    res.end();
  },

  /*Unblock user*/
  unBlockUser: async (req, res) => {
    let id = req.query.id;
    console.log(id,"iddddd")
    // block users
    try {
      let blockUser = await userModel.updateOne(
        { _id: id },
        { $set: { isBlocked: false } }
      );
      let response = commnFunction.checkRes(blockUser);
      response.message = "User unblock sucessfully";
      res.send(response);
    } catch (e) {
      logger.error(e);
      res.send({ status: false, code: 201, message: messages.ANNONYMOUS });
    }
    res.end();
  },

  // delete user
  deleteUser: async (req, res) => {
    let id = req.query.id;
    // block users
    try {
      let blockUser = await userModel.deleteOne(
        { _id: id }
    
      );

      let response = commnFunction.checkRes(blockUser);
      response.message = "User delete sucessfully";
      res.send(response);
    } catch (e) {
      console.log(e)
      logger.error(e);
      res.send({ status: false, code: 201, message: messages.ANNONYMOUS });
    }
    res.end();
  },

  //get user detail
  getUserdetail: async (req, res) => {
    let id = req.query.id;

    // get user detail
    try {
      let userDetail = await userModel.findOne({ _id: id }, {}, { lean: true });
      delete userDetail.password;
      delete userDetail.accessToken;
      let response = commnFunction.checkRes(userDetail);
      response.message = "";
      res.send(response);
    } catch (err) {
      logger.error(err);
      res.send({ status: false, code: 201, message: messages.ANNONYMOUS });
    }
    res.end();
  },

  uploadBanner:async(req,res)=>{
    try{
      const fileData = await commnFunction.fileUpload(req);
      console.log(fileData.image,'-----------')

      for(let i=0;i<fileData.image.length;i++){
        await new bannerModel({banner:fileData.image[i]}).save()
      }
      res.send({status:true, message:"banner uploaded successfully"})
      res.end()
    }catch(err){
      logger.error(err);
      console.log(err)
      res.send({ status: false, code: 201, message: messages.ANNONYMOUS });
    }
  },

  createSkills:async(req,res)=>{
    try{
      const skillName = req.body.skillName.toLowerCase();
      const isExist = await skillsModel.findOne({skillName:skillName});
      if(isExist) return res.status(400).send({sttaus:false, code:400, message:`${skillName} is already exists`})
      
      await skillsModel.create({skillName:skillName});
      res.status(200).send({status:true, code:200, message:" Skill creates successfully "});
    
    }catch(err){
      logger.error(err);
      res.send({ status: false, code: 201, message: messages.ANNONYMOUS });
    }
    res.end();
  }
};

function capitalize(s) {
  return s && s[0].toUpperCase() + s.slice(1).toLowerCase();
}
