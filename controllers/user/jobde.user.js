/**
 * File - jobde.user.js
 * desc - file contain all the user side functionality of jobde app 
 */


const jobdeUserModel = require('../../model/jobdeuser.model')
const commnFunction = require("../../common/commonFunction");
const logger = require("../../config/logger");
const jwt = require("jsonwebtoken");
const OtpModel = require("../../model/otps.model");
const postModel = require('../../model/posts.model');
const bannerModel = require('../../model/banner.model')
const categoryModel = require('../../model/category.model')




module.exports = {

    registerUser : async(req,res)=>{
        try{
            const body = req.body;
            console.log(body)
            const isMobileExist = await jobdeUserModel.findOne({countryCode:body.countryCode,mobileNumber:body.mobileNumber})
            if(isMobileExist) return res.status(400).send({status:false, message:`${body.mobileNumber} is already exist`});
            const isEmailExist = await jobdeUserModel.findOne({email:body.email})
            if(isEmailExist) return res.status(400).send({status:false, message:`${body.email} is already exist`});
            body.password = commnFunction.encrypt(body.password);
            console.log(isEmailExist,isMobileExist)
            let obj ={
              mobileNumber:body.mobileNumber,
              email:body.email,
              countryCode:body.countryCode,
              otp:1234,

            }
            console.log(obj,"obj------")
            let userData = await OtpModel.findOneAndUpdate(obj,{},{lean:true,new:true,upsert:true})
            let response = commnFunction.checkRes(userData);
            response.message = "user created";
            response.type=1
            res.send(response);
        }catch(err){
            logger.error(err);
            console.log(err)
            res.status(400).send({status:false, message:"Something went wrong"})
        }
    },

    login:async(req,res)=>{
        try{
            const body = req.body;
            console.log(body)
            const user = await jobdeUserModel.findOne({email:body.email})
            if(!user) return res.status(400).send({status:false, message:"User not found"});
            if(user.password === commnFunction.encrypt(body.password)) {
                let token = jwt.sign({fullName:user.fullName},process.env.TOKEN_SECRET,{expiresIn:'1h'});
                let data = await jobdeUserModel.findOneAndUpdate({email:user.email},{accessToken:token},{new:true})
                let response = commnFunction.checkRes(data);
                response.message = "Login Successfully";
                response.token= token;
                res.status(200).send(response);
            
            }else{
                res.status(400).send({status:false, message:"Password not matched"});
            }
        }catch(err){
            logger.error(err);
            console.log(err)
            res.status(400).send({status:false, message:"Something went wrong"})
        }
    },

    forgotPwd:async(req,res)=>{
        try{
          console.log(req.body)
            const {email,mobileNumber,countryCode} = req.body;
            const obj={}
            if(email) obj.email = email;
            if(mobileNumber) obj.mobileNumber = mobileNumber;
            const user = await jobdeUserModel.findOne(obj);
            if(!user) return res.status(400).send({status:false,code:400, message:`User not found`});
            var isExist = await OtpModel.findOne(obj);
            let otp = 1234;
            req.body.otp = otp;
            if (!isExist) await new OtpModel(req.body).save();
            // if(email){
            //     let message = `Hi ${user.fullName}<br>
            //         Your otp is ${otp}
            //     `
            //     commnFunction.sendMail(email,'Recover password',message)
            // }
            res.status(200).send({status:true,code:200,message:"otp sent successfully"})
        }catch(err){
            logger.error(err);
            console.log(err)
            res.status(400).send({status:false, message:"Something went wrong"})
        }
    },

    resendOtp:async(req,res)=>{
     try{
       const funcRes = await resendOtp(req);
       res.send(funcRes)
     }catch(err){
      logger.error(err);
      console.log(err)
      res.status(400).send({status:false, message:"Something went wrong"})
     }

    },
    verifyOtp:async(req,res)=>{
      try{
        let { type,otp} = req.body;
        let cond;
        console.log(type)
        let data
        if(type == 1){
          data = req.body.data;
          data = JSON.parse(data)
          console.log(data)

          cond = {$or:[{email:data.email},{mobileNumber:data.mobileNumber,countryCode:data.countryCode}]}
        }else{
          const {email,mobileNumber} = req.body
          if(email) cond.email = email;
          if(mobileNumber) cond.mobileNumber = mobileNumber;
        }
        console.log(cond)

        let getOtp = await OtpModel.findOne(cond);
        console.log(getOtp)

      if(!getOtp) return res.status(400).send({status:false, code:400, message:"Invalid user"})
        if(type == 1){ // 
            console.log(getOtp)
            if(getOtp.otp == otp){
              data.password = commnFunction.encrypt(data.password)
              await new jobdeUserModel(data).save();
              await OtpModel.deleteOne({_id:getOtp._id})
              res.send({status:true,code:200, message:"User registered successfully"});

            }else{
              res.send({status:false, code:400, message:"Invalid otp entered"})
            }
         

        }else if(type == 2){
          if(getOtp.otp == otp){
            await OtpModel.deleteOne({_id:getOtp._id})
            res.status(200).send({status:true, code:200, message:"Otp verified successfully"})
          }else{
            res.send({status:false, code:400, message:"Invalid otp entered"})

          }
        }else{
          res.status(400).send({status:false, code:400, message:"Invalid type"})
        }
      }catch(err){
        logger.error(err);
        console.log(err)
        res.status(400).send({status:false, message:"Something went wrong"})
      }
    },

    resetPassword:async(req,res)=>{
      try{
        const {email,countryCode,mobileNumber} = req.body;
        let obj = {};
        if(email) obj.email = email
        if(mobileNumber && countryCode){ obj.mobileNumber=mobileNumber; obj.countryCode= countryCode }
        let isExist = await jobdeUserModel.findOne(obj);
        if(!isExist) return res.send({status:false, code:400, message:'User not found'});
        let  password = commnFunction.encrypt( req.body.password);

        let updateData = await jobdeUserModel.findOneAndUpdate(obj,{password:password},{lean:true,new:true})
        let response = commnFunction.checkRes(updateData);
        response.code = 200;
        response.message = "Password changed successfully";
        res.send(response)


      }catch(err){
        logger.error(err);
        console.log(err)
        res.status(400).send({status:false, message:"Something went wrong"})
      }
    },





    /** ********************************************************************** POST API'S ********************************************************************************************** */

    createPost:async(req,res)=>{
      try{
        const user = req.body.userInfo;
        console.log(user._id,"====================")
        let fileData = await  commnFunction.fileUpload(req);
        let image  = fileData.image.length?fileData.image[0]:"" ;
        const obj = {
          image: image,
          title: fileData.fields.title || "",
          jobDate: fileData.fields.jobDate || "",
          price: fileData.fields.price || "",
          latitude: fileData.fields.latitude || "",
          longitude: fileData.fields.longitude || "",
          address: fileData.fields.address || "",
          description: fileData.fields.description || "",
          time: fileData.fields.time || "",
          userId: user._id

        }

        const savedData = await new postModel(obj).save();
        const response = commnFunction.checkRes(savedData);
        response.code = 200;
        response.message = "Post created successfully";
        res.send(response);

      }catch(err){
        logger.error(err);
        console.log(err)
        res.status(400).send({status:false, message:"Something went wrong"})
      }
    },

    userProfile:async(req,res)=>{
      const response = commnFunction.checkRes(req.body.userInfo)
      console.log(response)
      response.message = ""
      response.code=200;
      res.send(response)
      res.end()
    },

    /** get banners categories and posts */
    homePage:async(req,res)=>{
      try{
        const user  = req.body.userInfo
        const posts = await postModel.find({userId:user._id});
        const banners =  await bannerModel.find();
        const categories = await categoryModel.find();
        res.send({posts,banners,categories,status:true,code:200});
        res.end();
      }catch(err){
        logger.error(err);
        console.log(err)
        res.status(400).send({status:false, message:"Something went wrong"})
      }
    },
    getPosts:async(req,res)=>{
      try{
        const user  = req.body.userInfo
        const posts = await postModel.find({userId:user._id});
        const response = commnFunction.checkRes(posts)
        response.code = 200;
        res.message = ""
        res.send(response)
        res.end();
      }catch(err){
        logger.error(err);
        console.log(err)
        res.status(400).send({status:false, message:"Something went wrong"})
      }
    },
    getPostById:async(req,res)=>{
      try{
        console.log(req.query)
        const id =  req.query.id || req.params.id;
        const getPostData = await postModel.findOne({_id:id});
        const response = commnFunction.checkRes(getPostData);
        response.code = 200;
        response.message= "";
        res.send(response);

      }catch(err){
        logger.error(err);
        console.log(err)
        res.status(400).send({status:false, message:"Something went wrong"})
      }
    },

    updatePost:async(req,res)=>{
      try{
        
        const data = await update(postModel,req)
        data.message = "Post updated successfully"
        res.send(data)
      }catch(err){
        logger.error(err);
        console.log(err)
        res.status(400).send({status:false, message:"Something went wrong"})
      }
    },

    updateProfile:async(req,res)=>{
      try{
        // console.log(re)
        const data = await update(jobdeUserModel,req)
        data.message = "Profile updated successfully"
        res.send(data)

      }catch(err){
        logger.error(err);
        console.log(err)
        res.status(400).send({status:false, message:"Something went wrong"})
      }
    },

}



async function resendOtp(req){
   return new Promise(async(resolve,reject)=>{
    
      const {email,mobileNumber,countryCode} = req.body;
      console.log(req.body)
      const cond= {
        $or:[
          { 'email':email },
          { 'mobileNumber': mobileNumber,'countryCode':countryCode }
        ]
      }
      
      const isExist = await OtpModel.find(cond,{},{lean:true});
      let otp = 1234;
      isExist.otp = otp
      let updateOrsave = ''
      if(isExist.length){
        updateOrsave=await OtpModel.updateOne({_id:isExist[0]._id},{$set:isExist[0]},{lean:true,new:true}) 
      }else{
        let obj = {
          email:email,
          mobileNumber:mobileNumber,
          countryCode:countryCode,
          otp:otp,
        }
        updateOrsave=await new OtpModel(obj).save()

      }

     let response = commnFunction.checkRes(updateOrsave);
     response.message = "Otp send successfully";
     resolve(response)
      
    
   })
}

// check email or mobile exit or not
async function checkMobileEmail(req) {
    const body = req.body;
  
    const obj = {};
  
    if (body.mobileNumber != undefined || body.mobileNumber == "") {
      obj.mobileNumber = body.mobileNumber;
      obj.countryCode = body.countryCode;
    } else {
      obj.email = body.email;
    }
  
    const message =
      obj.mobileNumber != undefined
        ? `${body.mobileNumber} is already exist`
        : `${body.email} is already exist`;
  
    try {
      console.log(obj);
      const checkMobileOrEmail = await jobdeUserModel.findOne(obj);
      console.log(checkMobileOrEmail);
      if (checkMobileOrEmail)
        return { status: false, code: 400, message: message };
      return { status: true, code: 200, message: "" };
    } catch (err) {
      logger.error(err);
      return { status: false, code: 400, message: `` };
    }
}
  

async function update(model,req){
  return new Promise(async(resolve,reject)=>{
    const fileData = await commnFunction.fileUpload(req);
    console.log(fileData,"fjhsdgfjhdsgfjhdsgfjsdgfs")
    const image = fileData.image ? fileData.image[0]:"";
    const fields = fileData.fields;
    if(fileData.image)  fields.image = image;
    const id  = req.params.id;
    console.log(id)
    const updatedData = await model.updateOne({_id:id},{$set:fields},{lean:true, new:true});
    const response = commnFunction.checkRes(updatedData);
    response.code = 200;
    resolve(response)
  }).catch((err)=>{
    logger.error(err);
    console.log(err)
    return{status:false, message:"Something went wrong"}
  })
}