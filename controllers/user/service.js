/**
 * File Name: service.js
 * Date: 26-july-2021
 * Desc: this file contain all the logics for the user panel
 */

// Model Configration
const userModel = require("../../model/users.model");
const OtpModel = require("../../model/otps.model");
const subCategoryModel = require("../../model/subcategory.model");
const categoryModel = require("../../model/category.model");
const bannerModel = require('../../model/banner.model')
const postModel  = require('../../model/posts.model')

const logger = require("../../config/logger");
const messages = require("../../common/commonMessages").customMessages();
const commnFunction = require("../../common/commonFunction");
const jwt = require("jsonwebtoken");
const async = require("async");
const mongoose = require("mongoose");
const { ObjectId } = mongoose.Types;
module.exports = {
  // user sign up
  register: async (req, res) => {
    const body = req.body;
    const userInfo = req.body.userInfo;
    console.log(userInfo);
    // check email is exist or not
    // try {

    //   user = await userModel.findOne({ email: body.email });
    //   if (user) return res.send({ status: false, code: 400, message: `${body.email} is already exists`})
    // } catch (e) {
    //   logger.error(e)
    //   return res.send({ status: false, code: 400, message: message.BAD_REQUEST })
    // }

    //  // check mobile is exist or not
    //  try {

    //   isExist = await userModel.findOne({ mobileNumber: body.mobileNumber,countryCode:body.countryCode });
    //   if (isExist) return res.send({ status: false, code: 400, message: `${body.mobileNumber} is already exists`})
    // } catch (e) {
    //   logger.error(e)
    //   return res.send({ status: false, code: 400, message: message.BAD_REQUEST })
    // }

    // save user details
    try {
      // let checkEmail = await checkMobileEmail(req);
      // if(!checkEmail.status) return res.status(400).send(checkEmail)
      body.password = commnFunction.encrypt(body.password);
      let saveUser = await userModel.findOneAndUpdate(
        { _id: userInfo._id },
        { $set: body },
        { upsert: true, new: true }
      );
      let response = commnFunction.checkRes(saveUser);
      response.message = "Account created successfully";
      res.send(response);
    } catch (e) {
      logger.error(e);
      console.log(e);
      res.send({ status: false, code: 400, message: messages.ANNONYMOUS });
    }
  },

  // create account with email or number
  createUser: async (req, res) => {
    const body = req.body;
    let user;
    console.log(body, "users");
    // check email is exist or not
    try {
      user = await checkMobileEmail(req);
      if (user.status == false && user.code !== 200) return res.send(user);
      await new userModel(req.body).save();
      res.send(user);
    } catch (e) {
      console.log(e);
      logger.error(e);
      return res.send({
        status: false,
        code: 400,
        message: messages.BAD_REQUEST,
      });
    }
    // res.end()
  },

  // resend otp if user is not able to get otp
  resendOtp: async (req, res) => {
    const body = req.body;
    let cond = {};

    // check mobilenumber and is empty or not
    // if (body.countryCode == '' || body.countryCode == undefined) {
    //   return res.send({ status: false, code: 400, message: 'Country code is required' });
    // }else{
    //   if(body.email == "" || body.email ==undefined) return res.send({status:false, code:400, message:"Email is required"})
    // }

    // assign body parameter to cond object
    if (
      body.mobileNumber != undefined &&
      body.mobileNumber !== "" &&
      body.countryCode != undefined &&
      body.countryCode != ""
    ) {
      cond.mobileNumber = body.mobileNumber;
      cond.countryCode = body.countryCode;
    } else {
      if (body.email !== "" || body.email !== undefined)
        cond.email = body.email;
    }
    console.log(cond);
    // check mobile exist or not
    try {
      var isExist = await OtpModel.findOne(cond);
    } catch (e) {
      logger.error(e);
      return (
        res, json({ status: false, code: 400, message: messages.ANNONYMOUS })
      );
    }

    // var otp = commnFunction.genrateOtp(4);
    var otp = 1234;
    try {
      if (!isExist) await new OtpModel(body).save();
    } catch (e) {
      logger.error(e);
      console.log(e, "save otp");
      return res.json({
        status: false,
        code: 400,
        message: messages.ANNONYMOUS,
      });
    }

    try {
      var user = await userModel.findOne(cond);
      if (!user)
        return res.send({
          status: false,
          code: 400,
          message: "User not found",
        });
    } catch (err) {
      console.log(err, "find user");
      logger.error(err);
      return res.json({
        status: false,
        code: 400,
        message: messages.ANNONYMOUS,
      });
    }

    console.log("email-----------", cond.email);
    if (cond.email) {
      let htmlStr = `<p> <em> Hi ${cond["email"]},  </em> </p>
           <p> Your login otp is ${otp} </p>
           <p> Thanks & Regards  </p>
           <p>Team JobLe </p>`;

      try {
        await commnFunction.sendMail(cond.email, "Otp Verification", htmlStr);
      } catch (err) {
        logger.error(e);
        return (
          res, json({ status: false, code: 400, message: messages.ANNONYMOUS })
        );
      }
    }
    // await sendOtp('+12015524629',body.mobileNumber, otp,body.countryCode);

    OtpModel.updateOne(cond, { $set: { otp: 1234 } })
      .then((doc, err) => {
        if (err || !doc) {
          return res
            .status(201)
            .send({ status: true, code: 201, message: "Data Not Found" });
        } else {
          const response = commnFunction.checkRes(doc);
          delete response.results;
          response.message = "New otp sent on your mobile number";
          res.send(response);
        }
      })
      .catch((err) => {
        logger.error(err);
      });
  },

  // verify otp with registerd number
  verifyOtp: async (req, res) => {
    const body = req.body;
    let obj = {};
    let message = "";
    let token = "";
    if (body.mobileNumber) {
      obj.mobileNumber = body.mobileNumber;
      obj.countryCode = body.countryCode;
      message = "Invalid mobile number";
    } else {
      obj.email = body.email;
      message = "Invalid email address";
    }
    try {
      var isMobile = await OtpModel.findOne(obj);
      if (!isMobile)
        return res.send({ status: false, code: 202, message: message });
    } catch (e) {
      console.log(e, "errror000----");
      return res.send({
        status: false,
        code: 400,
        message: messages.ANNONYMOUS,
      });
    }

    if (body.otp == isMobile.otp) {
      try {
        await OtpModel.deleteMany({ mobileNumber: body.mobileNumber });
        const user = await userModel.findOne({
          mobileNumber: body.mobileNumber,
        });
        if (user) {
          if (body.verifyFor !== "update" || body.verifyFor !== "forgot") {
            token = jwt.sign(
              { mobileNumber: body.mobileNumber },
              process.env.TOKEN_SECRET
            );
            await userModel.findOneAndUpdate(
              { mobileNumber: body.mobileNumber },
              { accessToken: token, deviceToken: body.deviceToken }
            );
          }
          const response = commnFunction.checkRes(user);
          response.message = messages.VERIFIED;
          response.token = token;
          res.send(response);
        } else {
          return res.send({
            status: true,
            code: 200,
            message: "Verified Successfully",
          });
        }
      } catch (err) {
        logger.error(err);
        console.log(err, "errror000+++++++++++----");

        return res.send({
          status: false,
          code: 400,
          message: messages.ANNONYMOUS,
        });
      }
    } else {
      return res.send({
        status: false,
        code: 400,
        message: messages.INVALID_OTP,
      });
    }
    res.end();
  },

  //upload document
  uploadDocument: async (req, res) => {
    let formData = await commnFunction.fileUpload(req);
    let body = req.body.userInfo;
    console.log(formData, body);
    let images = formData.image;
    // upload document

    try {
      let upload = await userModel.findOneAndUpdate(
        { _id: body._id },
        { $set: { doucments: images } },
        { new: true }
      );
      let response = commnFunction.checkRes(upload);
      response.message = "Documents uploaded successfully";
      res.send(response);
    } catch (err) {
      logger.error(err);
      return res.send({
        status: false,
        code: 400,
        message: messages.ANNONYMOUS,
      });
    }
    res.end();
  },

  // get profession Types
  getProfessionType: async (req, res) => {
    // get categories
    try {
      let categories = await categoryModel.aggregate([
        { $project: { _id: 1, categoryName: 1, skillType: 1} },
      ]);
      let response = commnFunction.checkRes(categories);
      response.message = "";
      res.send(response);
    } catch (err) {
      logger.error(err);
      return res.send({
        status: false,
        code: 400,
        message: messages.ANNONYMOUS,
      });
    }
  },

  // get categories
  getCategories: async (req, res) => {
    let professionType = req.query.type;
    // get categories
    try {
      let categories = await categoryModel.aggregate([
        { $match: { skillType: professionType } },
      ]);
      let response = commnFunction.checkRes(categories);
      response.message = "";
      res.send(response);
    } catch (err) {
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  },

  // get sub categories
  getSubCategories: async (req, res) => {
    let subId = req.query.category_id;

    //get sub category
    try {
      let getCategories = await subCategoryModel.aggregate([
        { $match: { categoryId: subId } },
      ]);
      let response = commnFunction.checkRes(getCategories);
      response.message = "";
      res.send(response);
    } catch (err) {
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  },

  // save experience
  saveExperince: async (req, res) => {
    let userInfo = req.body.userInfo;
    let experience = await commnFunction.fileUpload(req);
    console.log(experience);
    experience = experience.fields;
    try {
      let saveData = await userModel.findOneAndUpdate(
        { _id: userInfo._id },
        { $set: { experience: experience } },
        { upsert: true, new: true }
      );
      let response = commnFunction.checkRes(saveData);
      response.message = "";
      res.send(response);
    } catch (e) {
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  },

  // check username exist or not
  checkUserName: async (req, res) => {
    let username = req.body.username;

    try {
      let check = await userModel.find({ username: username }, {});
      console.log(check);
      if (!check.length)
        return res.send({
          status: true,
          code: 200,
          message: `${username} is available`,
        });
      else
        return res.send({
          status: false,
          code: 400,
          message: `${username} is already registerd`,
        });
    } catch (e) {
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  },

  // login
  logIn: async (req, res) => {
    let body = req.body;
    let query = {
      $or: [
        { username: body.username },
        { email: body.username },
        { countryCode: body.countryCode, mobileNumber: body.username },
      ],
    };

    try {
      console.log(query, "--");
      let getData = await userModel.find(query, {}, { lean: true });
      console.log(getData, "data============");
      if (getData.length) {
        if (getData[0].password == commnFunction.encrypt(body.password)) {
          const token = jwt.sign({email:getData[0].email,id:getData[0]._id}, process.env.TOKEN_SECRET);
          await userModel.findOneAndUpdate(
            { _id: getData[0]._id },
            { accessToken: token, deviceToken: body.deviceToken }
          );
          delete getData[0].password;
          const response = commnFunction.checkRes(getData);
          response.message = messages.VERIFIED;
          response.token = token;
          res.send(response);
        } else {
          return res.send({
            status: false,
            code: 400,
            message: "Password not matched",
          });
        }
      } else {
        return res.send({
          status: false,
          code: 400,
          message: "User not found",
        });
      }
    } catch (err) {
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  },

  // education document
  uploadEducationDocs: async (req, res) => {
    let formData = await commnFunction.fileUpload(req);
    let images = formData.image;
    let fields = formData.fields;
    fields.educationDocs = images;
    let userInfo = req.body.userInfo;
    try {
      let updateInfo = await userModel.findOneAndUpdate(
        { username: userInfo.username },
        { $set: { education: fields } },
        { upsert: true, new: true }
      );
      let response = commnFunction.checkRes(updateInfo);
      response.message = "";
      res.send(response);
    } catch (err) {
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  },

  // salary expectation
  salaryExpectation: async (req, res) => {
    let body = req.body;
    let user = req.body.userInfo;

    try {
      let saveSalary = await userModel.findOneAndUpdate(
        { username: user.username },
        { $set: { salary: body } },
        { upsert: true, new: true }
      );
      let response = commnFunction.checkRes(saveSalary);
      response.message = "";
      res.send(response);
    } catch (err) {
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  },

  // change Password
  changePassword: async (req, res) => {
    let body = req.body;
    console.log(body, "bodfy===");
    let query = {
      $or: [
        { username: body.username },
        { email: body.email },
        { countryCode: body.countryCode, mobileNumber: body.mobileNumber },
      ],
    };

    try {
      if (body.newPassword == body.confirmPassword) {
        body.password = commnFunction.encrypt(String(body.password));
        let getData = await userModel.updateOne(
          query,
          { $set: { password: body.password } },
          { upsert: true, new: true, lean: true }
        );
        const response = commnFunction.checkRes(getData);
        delete response.results;
        response.message = "Password changed successfully";
        res.send(response);
      } else {
        return res.send({
          status: false,
          code: 400,
          message: "Password not matched",
        });
      }
    } catch (err) {
      logger.error(err);
      console.log(err, "--------");
      return res.send({ status: false, code: 400, message: err.message });
    }
  },

  // save category and sub category
  saveCategories: async (req, res) => {
    let user = req.body.userInfo;
    let body = req.body;
    try {
      let update = await userModel.findOneAndUpdate(
        { _id: user._id },
        { $set: body },
        { new: true }
      );
      let response = commnFunction.checkRes(update);
      response.message = "";
      res.send(response);
    } catch (err) {
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  },

  userList: async (req, res) => {
    try {
      let users = await userModel.find();
      let response = commnFunction.checkRes(users);
      response.message = "";
      res.send();
    } catch (err) {
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  },

  userProfile:async(req,res)=>{
    try{
      const id = req.body.userInfo._id;
      console.log(req.body.userInfo,"-----------")
      const userData = await userModel.findOne({_id:id});
      const response = commnFunction.checkRes(userData);
      response.message = "",
      res.send(response)
    }catch(err){
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  },
  
  homePage:async(req,res)=>{
    try{
      const user  = req.body.userInfo;
      const banners = await bannerModel.find();
      const post  = await postModel.find();
      res.status(200).send({status:true, code:200, data:{post:post,banners:banners}})
    }catch(err){
      logger.error(err);
      return res.send({ status: false, code: 400, message: err.message });
    }
  }
};

// check email or mobile exit or not
async function checkMobileEmail(req) {
  const body = req.body;

  const obj = {};

  if (body.mobileNumber != undefined || body.mobileNumber == "") {
    obj.mobileNumber = body.mobileNumber;
    obj.countryCode = body.countryCode;
  } else {
    obj.email = body.email;
  }

  const message =
    obj.mobileNumber != undefined
      ? `${body.mobileNumber} is already exist`
      : `${body.email} is already exist`;

  try {
    console.log(obj);
    const checkMobileOrEmail = await userModel.findOne(obj);
    console.log(checkMobileOrEmail);
    if (checkMobileOrEmail)
      return { status: false, code: 400, message: message };
    return { status: true, code: 200, message: "" };
  } catch (err) {
    logger.error(err);
    return { status: false, code: 400, message: `` };
  }
}
