/**
 *  FileName: index.js
 *  Author: Pankaj kumar
 *  Date: 16 june, 2021
 *  Description: index.js is the main file of the Application it's include all the file
 *  of project
 */

require("dotenv").config();
require("./config/db_connection");
const morgan = require("morgan");
const express = require("express");
const port = process.env.PORT || 5001;
const terminate = require("./terminate");
const logger = require("./config/logger");
const adminRoute = require("./routes/adminRoutes/admin.routes");
const userRoute = require("./routes/userRoutes/user.routes");

const defaultAdminUser = require("./config/default_user");
const swaggerJsDocs = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
// const path = require('./routes/userRoutes/user.routes')
const cors = require("cors");
const app = express();
console.log(process.env.DB_LOCAL_PORT);
if (process.env.NODE_ENV == "developement")
  serverUrl = `http://${process.env.DB_PROD_PORT}:6001`;
else serverUrl = `http://${process.env.DB_PROD_PORT}:6001`;
const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Testing",
      version: "1.0.0",
      description: "A simple Express Testing API",
    },
    servers: [{ url: serverUrl }],
  },
  apis: ["./routes/userRoutes/user.routes.js"],
};
const swaggerSpecification = swaggerJsDocs(options);
let server = app.listen(port, () =>
  logger.info(`Server running on port ${port}`)
);
let sessionoptions = {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
let url = `mongodb://localhost:27017/${process.env.DB_LOCAL}`;
let store = new MongoDBStore({
  uri: url,
  sessionoptions,
  collection: "sessions",
});

store.on("error", function (error) {
  // console.log(error)
  logger.error(error);
});

app.use(
  require("express-session")({
    secret: process.env.SESSION_SECRET,
    cookie: {
      maxAge: 1000 * 60 * 60 * 1, // 1 hour
    },
    store: store,
    resave: false,
    saveUninitialized: false,
  })
);
app.use(morgan("combined", { stream: logger.stream }));
defaultAdminUser.defaultUser(); // Created Default Admin User
app.use(cors());
app.use(express.json());
app.use(adminRoute);
app.use(userRoute);
app.use(express.static("src/templates"));
app.use(express.static("uploads"));
app.use("/", swaggerUi.serve, swaggerUi.setup(swaggerSpecification));

const exitHandler = terminate(server, {
  coredump: false,
  timeout: 500,
});

process.on("uncaughtException", exitHandler(1, "Unexpected Error"));
process.on("unhandledRejection", exitHandler(1, "Unhandled Promise"));
process.on("SIGTERM", exitHandler(0, "SIGTERM"));
process.on("SIGINT", exitHandler(0, "SIGINT"));
