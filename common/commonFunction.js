/*
    File: commonFunction.js
    Author: Pankaj kumar
    date: 16,june,2021
    This file include the common function of application.
*/
const crypto = require('crypto');
const email = require('nodemailer')
const logger = require('../config/logger')
const formidable = require('formidable');
const fs =require('fs');
const path = require('path')
module.exports={

    encrypt: (text) => {
        let mykey = crypto.createCipher('aes-128-cbc', process.env.SECRET);
        let mystr = mykey.update(text, 'utf8', 'hex');
        mystr += mykey.final('hex');
        return mystr;
      },
    
      decrypt: (text) => {
        const mykey = crypto.createDecipher('aes-128-cbc', process.env.SECRET);
        let mystr = mykey.update(text, 'hex', 'utf8');
        mystr += mykey.final('utf8');
        return mystr;
      },
    
      checkRes: (data) => {
        const obj = { status: true, code: 200 };
        if (Array.isArray(data)) {
          obj.data = data;
        } else {
          obj.results = data;
        }
        return obj;
      },
    
      
    
      randomString: (length) => {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < length; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
      },
    
      sendMail: async (to, subject, message) => {
        const nodemailer = email;
        // var mailData = {
        // from: 'pankajpaul0007@gmail.com',
        // to: to,
        // subject: subject,
        // html: message
        // };
        // mailgun.messages().send(mailData, function (err, response) {
        // if (err) logger.error(err)
        // else console.log(response)
        // })
        const testAccount = await nodemailer.createTestAccount();
        console.log(testAccount, '=====');
        const transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: 'joble.infynex@gmail.com',
            pass: 'Infynex123',
          },
        });
        const mailOptions = {
          from: 'joble.infynex@gmail.com',
          to: to,
          subject: subject,
          html: message,
        };
        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            logger.error(error);
            console.log(error);
          } else {
            logger.info(info);
            console.log('Email sent:' + info.response);
          }
        });
      },
    
      uniqueStr(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
      },
    
      genrateOtp: (length) => {
        let result = '';
        const characters = '0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
      },

      fileUpload: (req) => {
       
        const form = formidable.IncomingForm();
        // const path = forpath;
        form.multiples = true;
        const imageArray = [];
        // const str = 'data:image/jpeg;base64,';
        var oldPath = '';
        var base64 = '';
        var extension = '';
        let file = '';
        var newPath = '';
        let uniqueStr = '';
        return new Promise((resolve, reject) => {
          form.parse(req, async (err, fields, files) => {
            // console.log(req, '===========================')
            files.file = Array.isArray(files.file) ? files.file : [files.file];
            if (!files.file[0]) return resolve({ status: false, fields: fields });
            if (err) {
              console.log(err, 'errrr');
              return logger.error(err);
            }
            if (!fs.existsSync(path.join(process.cwd(), 'uploads'))) fs.mkdirSync(process.cwd() + '/uploads');
            if (fields.userId) {
              if (!fs.existsSync(path.join(process.cwd() + '/uploads', fields.userId))) fs.mkdirSync(path.join(process.cwd() + '/uploads', fields.userId));
            }
    
            for (let i = 0; i < files.file.length; i++) {
              oldPath = files.file[i].path;
              base64 = base64Encode(oldPath);
              extension = path.extname(files.file[i].name);
              file = path.basename(files.file[i].name, extension);
              // console.log(file,'===============');
              uniqueStr = uniqueString(3);
              file = Date.now() + '_' + uniqueStr + extension;
              if (fields.userId) newPath = path.join(process.cwd(), 'uploads') + '/' + fields.userId + '/' + file;
              else newPath = path.join(process.cwd(), 'uploads') + '/' + file;
              // const backUpPath = path.join(process.cwd(), 'backUploads') + '/' + fields.userId + '/' + file;
              console.log(newPath)
              imageArray.push(file);
              fs.writeFile(newPath, base64, 'base64', function (err) {
                if (err) console.log(err);
                else console.log('Image Upload sucessfully')
              });
              // fs.writeFile(backUpPath, base64, 'base64', function (err) {
              //   if (err) console.log(err);
              //   else console.log('Image Upload sucessfully')
              // });
            }
            resolve({ status: true, message: 'uploaded successfully', image: imageArray, fields: fields });
          });
        });
      },
    
      sendNotification: (message) => {
        fcm.send(message, function (err, response) {
          if (err) {
            logger.error(err);
            console.log(err, '====errrrrr========');
          } else {
            logger.info(response);
            // console.log(response,'============');
          }
          return response;
        });
      },
    
      sendOtp: (req) => {
        client.messages.create({
          body: `Please use this email` + req.body.email + ` & invite code :` + req.body.otp + ` to login.
            Thanks Profession EntWined Team.`,
          to: req.phone, // Text this number
          from: '', // From a valid Twilio number
        }, function (err, message) {
          if (err) {
            logger.error(err)
          } else {
            if (message.sid) {
              console.log("Suuccess------", message.sid)
            } else {
              console.log("Error------", message)
            }
          }
        });
      },
    
      profession: () => {
        let profession = [
          'Accountant', 'Actor', 'Actuary', 'Administrative Assistant', `Agriculturist`, `Air Traffic Controller`, `Anesthesiologist`, `Animal Trainer`, `Arborist`, `Architect`, `Armed Forces Service member`, `Artist`, `Assistant`, `Astronaut`, `Astronomer`, `Athlete`, `Attendant`, `Audiologist`, `Author`,
          `Bartender`, `Beautician`, `Biologist`, `Botanist`, `Business Owner`,
          `Cafeteria Worker`, `Captain`, `Carpenter`, `Cashier`, `Certified Nurse Midwife`, `Certified Nursing Assistant`, `Chef`, `Chemist`, `Chief Executive Officer`, `Chief Financial Officer`, `Child Care Provider`, `Chiropractor`, `Civil Engineer`, `Clergy`, `Clerk`, `Coach`, `Commissioner`, `Construction Worker`, `Consultant`, `Cosmetologist`, `Counselor`, `Court Reporter`,
          `Deejay`, `Dental Hygienist`, `Dentist`, `Designer`, `Dietician`, `Director`, `Doctor`, `Driver`,
          `Ecologist`, `Economist`, `Editor`, `Educator`, `Electrical Worker`, `Emergency Medical Technician`, `Engineer`, `Esthetician`,
          `Farmer`, `Financial Advisor`, `Firefighter`, `Florist`,
          `Geologist`, `Graphic Designer`, `Guidance Counselor`, `Gynecologist`,
          `Hairdresser`, `Horticulturist`, `Human Resources`,
          `Immunologist`, `Insurance Agent`, `Interpreter`, `Investor`, `IT Professional`,
          `Janitor`, `Jeweler`, `Journalist`, `Judge`,
          `Lawyer`, `Librarian`,
          `Maintenance Worker`, `Makeup Artist`, `Manager`, `Marketing`, `Massage Therapist`, `Mathematician`, `Medical Assistant`, `Meteorologist`, `Mortician`, `Musician`,
          `Nail Technician`, `Nanny`, `Nurse`,
          `Obstetrician`, `Occupational Therapist`, `Optometrist`,
          `Paleontologist`, `Paralegal`, `Park Ranger`, `Pastor`, `Pathologist`, `Pediatrician`, `Personal Assistant`, `Personal Trainer`, `Pharmacist`, `Photographer`, `Physical Therapist`, `Physician`, `Physician’s Assistant`, `Physicist`, `Pilot`, `Police Officer`, `Politician`, `Postal Worker`, `President`, `Priest`, `Principal`, `Producer`, `Professor`, `Programmer`, `Proofreader`, `Proprietor`, `Psychiatric Nurse`, `Psychiatrist`, `Psychologist`,
          `Radiologist`, `Realtor`, `Repair Worker`, `Reporter`, `Retail Worker`,
          `Salesperson`, `Scientist`, `Secretary`, `Server`, `Singer`, `Small-Business Owner`, `Social Worker`, `Sociologist`, `Speech Therapist`, `Spy`, `Statistician`, `Stenographer`, `Surgeon`, `Surveyor`,
          `Tailor`, `Teacher`, `Technical Writer`, `Technician`, `Therapist`, `Tour Guide`, `Trainer`, `Translator`, `Travel Agent`, `Truck Driver`,
          `Underwriter`,
          `Veterinarian`, `Videographer`, `Virologist`,
          `Waitstaff`, `Web Designer, Writer`,
          `Zookeeper`, `Zoologist`
        ]
        return profession;
      },
    
      strToArray: (str) => {
        console.log(str, "aghds")
        var arr = [];
        var str_array = str.split(',');
        for (var i = 0; i < str_array.length; i++) {
          // Trim the excess whitespace.
          str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
          // Add additional code here, such as:
          arr.push(str_array[i])
        }
        return arr;
      }
    };
    
    function base64Encode(file) {
      // read binary data
      const bitmap = fs.readFileSync(file);
      // convert binary data to base64 encoded string
      const base64 = Buffer.from(bitmap, 'base64').toString('base64');
      return base64;
    }
    
    function uniqueString(length) {
      let result = '';
      const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      const charactersLength = characters.length;
      for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      return result;
    }
    