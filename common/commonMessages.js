/**
 *  FileName: commonMessages.js
 *  Author: Pankaj kumar
 *  Date: 16 june, 2021
 */

 module.exports = {
    customMessages: () => {
        const messages = {
          DATA_NOT_FOUND: 'Data Not Found',
          CREATION: 'Created Successfully',
          ERROR: 'Error',
          UPDATE: 'Updated Successfully',
          DELETE: 'Deleted Successfully',
          VERIFIED: 'Verified Successfully',
          NOT_MATCHED: 'Password not matched',
          USER_NOT_FOUND: 'User not found',
          LOGIN: 'Login Successfully',
          OTP_NOT_MATCHED: 'OTP not matched',
          VERFIED: 'Verified Successfull',
          ANNONYMOUS: 'Something went wrong',
          ERROR: 'Error',
          SESSION_ERROR: 'Session Expired',
          BLOCKED_BY_ADMIN: 'Blocked by Admin',
          INVALID_PATH: 'Invalid path access',
          OTP_SENT: 'Otp sent on your registered email id ',
          FIELD_ERR: 'is Required',
          PWD_CHANGE_ERR: 'Unable to change password',
          PWD_CHANGED: 'Password changed successfully',
          OLD_PWD: 'Old password is not recoginezed',
          PWD_ON_EMAIL: 'Password sent on your registered email.',
          FETCH: 'Data fetched successfully',
          EMAIL_NOT_VERIFIED: 'Please Verify Your Email',
          JOB_CREATED: 'Job created successfully',
          EMAIL_EXIST: 'Email is already exist',
          PHONE_EXIST: 'Phone number is already exist',
          PHONE_NOT_EXIST: 'Invalid Phone Number',
          REGISTER: 'Registered Successfully',
          NEW_OTP: 'NEW OTP',
          INVALID_OTP: 'Invalid OTP',
          EMAIL_NOT_EXIST: 'Email not exist',
          ACOOUNT_BLOCKED: 'Your account is blocked. Please contact the administrator at pankajk.zoptal@gmail.com',
          OLD_PASSWORD: 'Old password is not recognized',
          BAD_REQUEST:"Bad Request",
          Exist:"Already Exist",
          // SUB_CAT_EXIST:"Sub category already Exist",
          CATEGORY_ADDED:"Category added Successfully",
          SUB_CAT_ADDED:"Sub category added Successfully",
          CAT_NOT_FOUND:"Category Not Found",
          NOT_FOUND:"Not Found",
        };
        return messages;
      }
 }