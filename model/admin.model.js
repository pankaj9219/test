const mongoose = require('mongoose');
const schema = mongoose.Schema({
    firstName: { type: String, trim: true },
    lastName: { type: String, trim: true },
    email: { type: String, trim:true ,index:true},
    mobileNumber: { type: Number },
    otp:{type:String,default:''},
    password: { type: String }
})

const model = mongoose.model('admins',schema);
module.exports = model