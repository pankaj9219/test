/**
 * FileName :  posts.model.js
 * Date     :  31 july 2021
 * Author   :  Pankaj Kumar
 * Desc     :  contain information regarding skills created by jobde user
 */

 const mongoose = require('mongoose');
 const schema = mongoose.Schema({
 
     skillName               :       { type: String, trim: true },
     created_at              :       { type: Number,  default: Date.now()},
     updated_at              :       { type: Number,  default: Date.now()}
     
 })
 
 const model = mongoose.model('skills',schema);
 module.exports = model;
 