/**
 * file: banners.model.js
 * desc: this file contains the modal of category  collection. Admin will add unique category .
 * date:02-july-2021
 */

 const mongoose = require('mongoose');
 const schema = mongoose.Schema({
     banner: { type: String, default: ""},
     createdAt: { type: Number, default: Date.now() },
     updatedAt: { type: Number, default: Date.now() }
 
 })
 
 const model = mongoose.model('banners', schema);
 module.exports = model;