/**
 * file: subcategory.model.js
 * desc: this file contains the modal of SubCategory  collection. Admin will add unique Subcategory under category .
 * date:02-july-2021
 */

const mongoose = require('mongoose');
const schema = mongoose.Schema({
    categoryId              :        { type: String, trim: true },
    subCategory             :        { type: String, trim: true },
    skillType               :        { type: String, trim: true },
    categoryName            :        { type: String, trim: true },
    category                :        { type: String, trim: true },
    images                  :        { type: String, trim: true, default:""},
    createdAt               :        { type: Number, default: Date.now()},
    updatedAt               :        { type: Number, default: Date.now()}

})

const model = mongoose.model('subcategories', schema);
module.exports =model
