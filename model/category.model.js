/**
 * file: category.model.js
 * desc: this file contains the modal of category  collection. Admin will add unique category .
 * date:02-july-2021
 */

const mongoose = require('mongoose');
const schema = mongoose.Schema({
    categoryName: { type: String, trim: true, index: true },
    categoryImage: { type: String, trim: true, default:"default.jpeg" },

    skillType:{ type:String, trim:true},
    createdAt: { type: Number, default: Date.now() },
    updatedAt: { type: Number, default: Date.now() }

})

const model = mongoose.model('categories', schema);
module.exports = model;