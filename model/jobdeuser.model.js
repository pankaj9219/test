/**
 * FileName :  jobdeuser.model.js
 * Date     :  31 july 2021
 * Author   :  Pankaj Kumar
 * Desc     :  contain jobde user information for app user
 */

 const mongoose = require('mongoose');
 const schema = mongoose.Schema({
 
     fullName                :       { type: String, trim: true },
     email                   :       { type: String, trim:true, index:true},
     password                :       { type: String, default: "", index:true},
     mobileNumber            :       { type: String, trim:true, default: ""},
     countryCode             :       { type: String, trim:true, default:''},
     countryCodeText         :       { type: String, trim:true, default:''},
     isBlocked               :       { type: Boolean, default:false},
     ipAddress               :       { type: String, trim:true, default:""},
     IMEINumber              :       { type:String,  trim:true, default:""},
     deviceType              :       { type: String, trim:true, default:''},
     deviceToken             :       { type: String, trim:true, default:''},
     deviceModel             :       { type: String, trim:true, default:''},
     appVersion              :       { type: String, trim:true, default:''},
     accessToken             :       { type: String, trim:true, default:''},
     image                   :       { type: String, trim:true, default:''},
     alternateMobileNumber   :       { type: String, trim:true, default:''},
     created_at              :       { type: Number,  default: Date.now()},
     updated_at              :       { type: Number,  default: Date.now()}
 
 
 })
 
 const model = mongoose.model('jobdeusers',schema);
 module.exports = model
 