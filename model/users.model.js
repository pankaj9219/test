/**
 * FileName :  user.model.js
 * Date     :  31 july 2021
 * Author   :  Pankaj Kumar
 * Desc     :  contain user information for app user
 */

const mongoose = require('mongoose');
const schema = mongoose.Schema({

    fullName                :       { type: String, trim: true },
    email                   :       { type: String, trim:true, index:true},
    password                :       { type: String, default: "", index:true},
    mobileNumber            :       { type: String, trim:true, default: ""},
    countryCode             :       { type: String, trim:true, default:''},
    countryCodeText         :       { type: String, trim:true, default:''},
    permanentAddress        :       { type: String, trim:true, default:''},
    permanentState          :       { type: String, trim:true, default:''},
    permanentZipCode        :       { type: Number, default:0},
    presentAddress          :       { type: String, trim:true, default:''},
    presentState            :       { type: String, trim:true, default:''},
    presentZipCode          :       { type: Number,  default:0},
    isTermAndPolicy         :       { type: Boolean, default:false},
    isBlocked               :       { type: Boolean, default:false},
    isAccountVerified       :       { type: Boolean, default:false},
    categoryId              :       { type: String, trim:true, default:"" },
    subcatId                :       { type: String, trim:true, default:"" },
    skillType               :       { type: String, trim:true, default:"" },
    ipAddress               :       { type: String, trim:true, default:""},
    IMEINumber              :       { type:String,  trim:true, default:""},
    deviceType              :       { type: String, trim:true, default:''},
    deviceToken             :       { type: String, trim:true, default:''},
    deviceModel             :       { type: String, trim:true, default:''},
    appVersion              :       { type: String, trim:true, default:''},
    categoryName            :       { type: String, trim:true, default:''},
    subCategory             :       { type: String, trim:true, default:''},
    doucments               :       { type: Array,  default:[]},
    accessToken             :       { type: String, trim:true, default:''},
    experience              :       { type: Object, default:{}},
    education               :       { type: Object, default:{}},
    salary                  :       { type: Object, default:{}},
    username                :       { type: String, trim:true, default:"",index:true},
    created_at              :       { type: Number,  default: Date.now()},
    updated_at              :       { type: Number,  default: Date.now()}


})

const model = mongoose.model('users',schema);
module.exports = model
