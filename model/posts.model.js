/**
 * FileName :  posts.model.js
 * Date     :  31 july 2021
 * Author   :  Pankaj Kumar
 * Desc     :  contain information regarding posts created by jobde user
 */

 const mongoose = require('mongoose');
 const schema = mongoose.Schema({
 
     image                   :       { type: String, trim: true },
     title                   :       { type: String, trim:true},
     jobDate                 :       { type: String, default: ""},
     userId                  :       { type: String, default: "", index: true},
     price                   :       { type: String, trim:true, default: ""},
     latitude                :       { type: String, trim:true, default:''},
     longitude               :       { type: String, trim:true, default:''},
     address                 :       { type: String, trim:true, default:''},
     description             :       { type: String, trim:true, default:''},
     time                    :       { type: String, trim:true, default:''},

 
 
 })
 
 const model = mongoose.model('posts',schema);
 module.exports = model
 