const mongoose = require('mongoose');
const schema = mongoose.Schema({
 
    
    mobileNumber            :       { type: Number, default: 0, index:true},
    countryCode             :       { type:String,  default:''},
    countryCodeText         :       { type:String,  default:''},
    otp                     :       { type:Number,  default:0},
    email                   :       { type:String,  default:""},
    created_at              :       { type:Number,  default:0},
    updated_at              :       { type:Number,  default:0},
   

})

const model = mongoose.model('otps',schema);
module.exports = model
